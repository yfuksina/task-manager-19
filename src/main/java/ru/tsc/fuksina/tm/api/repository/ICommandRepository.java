package ru.tsc.fuksina.tm.api.repository;

import ru.tsc.fuksina.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    Collection<AbstractCommand> getTerminalCommands();

    void add (AbstractCommand command);

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArgument(String argument);

}
