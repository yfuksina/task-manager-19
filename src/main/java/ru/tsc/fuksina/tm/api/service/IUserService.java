package ru.tsc.fuksina.tm.api.service;

import ru.tsc.fuksina.tm.enumerated.Role;
import ru.tsc.fuksina.tm.model.User;

public interface IUserService extends IService<User> {

    User findOneByLogin(String login);

    boolean isLoginExists(String login);

    User findOneByEmail(String email);

    boolean isEmailExists(String email);

    User removeByLogin(String login);

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User setPassword(String userId, String password);

    User updateUser(
            String userId,
            String firstName,
            String lastName,
            String middleName
    );

}
