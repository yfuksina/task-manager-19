package ru.tsc.fuksina.tm.api.repository;

import ru.tsc.fuksina.tm.model.Project;

public interface IProjectRepository extends IRepository<Project> {

    Project create(String name);

    Project create(String name, String description);

}
