package ru.tsc.fuksina.tm.api.service;

import ru.tsc.fuksina.tm.enumerated.Status;
import ru.tsc.fuksina.tm.model.Task;

import java.util.Date;
import java.util.List;

public interface ITaskService extends IService<Task> {

    Task create(String name);

    Task create(String name, String description);

    Task create(String name, String description, Date dateStart, Date dateEnd);

    List<Task> findAllByProjectId(String projectId);

    Task updateByIndex(Integer index, String name, String description);

    Task updateById(String id, String name, String description);

    Task changeTaskStatusByIndex(Integer index, Status status);

    Task changeTaskStatusById(String id, Status status);

}
