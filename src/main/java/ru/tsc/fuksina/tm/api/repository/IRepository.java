package ru.tsc.fuksina.tm.api.repository;

import ru.tsc.fuksina.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    List<M> findAll();

    List<M> findAll(Comparator comparator);

    M add(M model);

    M findOneByIndex(Integer index);

    M findOneById(String id);

    M remove(M model);

    M removeByIndex(Integer index);

    M removeById(String id);

    void clear();

    boolean existsById(String id);

    int getSize();

}
