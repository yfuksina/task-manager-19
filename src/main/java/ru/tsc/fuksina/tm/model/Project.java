package ru.tsc.fuksina.tm.model;

import ru.tsc.fuksina.tm.api.model.IWBS;
import ru.tsc.fuksina.tm.enumerated.Status;
import ru.tsc.fuksina.tm.util.DateUtil;

import java.util.Date;

public final class Project extends AbstractModel implements IWBS {

    private String name = "";

    private String description = "";

    private Status status = Status.NOT_STARTED;

    private Date created = new Date();

    private Date dateStart;

    private Date dateEnd;

    public Project() {
    }

    public Project(final String name, final Status status, final String description, final Date dateStart) {
        this.name = name;
        this.status = status;
        this.description = description;
        this.dateStart = dateStart;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    @Override
    public String toString() {
        return name + " : " +
                getStatus().getDisplayName() + " : " + description +
                " : Created : " + DateUtil.toString(getCreated()) +
                " : Start Date : " + DateUtil.toString(getDateStart());
    }

}
