package ru.tsc.fuksina.tm.repository;

import ru.tsc.fuksina.tm.api.repository.ITaskRepository;
import ru.tsc.fuksina.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public Task create(final String name) {
        final Task task = new Task();
        task.setName(name);
        return add(task);
    }

    @Override
    public List<Task> findAllByProjectId(String projectId) {
        final List<Task> result = new ArrayList<>();
        for (Task task : models) {
            if (task.getProjectId() == null) continue;
            if (task.getProjectId().equals(projectId)) result.add(task);
        }
        return result;
    }

    @Override
    public Task create(final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }

}
