package ru.tsc.fuksina.tm.repository;

import ru.tsc.fuksina.tm.api.repository.IUserRepository;
import ru.tsc.fuksina.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findOneByLogin(final String login) {
        for (final User user:models) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findOneByEmail(final String email) {
        for (final User user:models) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findOneByLogin(login);
        if (user == null) return null;
        return remove(user);
    }

}
