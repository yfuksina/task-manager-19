package ru.tsc.fuksina.tm.command.user;

import ru.tsc.fuksina.tm.model.User;

public final class UserShowProfileCommand extends AbstractUserCommand {

    public static final String NAME = "user-show-profile";

    public static final String DESCRIPTION = "Show user profile";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW USER PROFILE]");
        final User user = serviceLocator.getAuthService().getUser();
        showUser(user);
    }

}
