package ru.tsc.fuksina.tm.command.system;

import ru.tsc.fuksina.tm.api.service.ICommandService;
import ru.tsc.fuksina.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    public ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
